-- CreateTable
CREATE TABLE "subscription" (
    "channel_id" TEXT NOT NULL,
    "guild_id" TEXT NOT NULL,

    PRIMARY KEY ("channel_id")
);

-- CreateTable
CREATE TABLE "live_notification" (
    "sport" TEXT NOT NULL,
    "category" TEXT NOT NULL,
    "modality" TEXT NOT NULL,
    "startHour" TEXT NOT NULL,
    "startDate" TEXT NOT NULL,

    PRIMARY KEY ("sport","category","modality","startHour","startDate")
);
