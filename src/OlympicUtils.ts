import { AnyEventType, ConfrontationEvent, GenericEvent, MatchEvent, SoccerMatchEvent } from "./OlympicQueryData";

class OlympicUtils {
  static isSoccerEvent (arg: AnyEventType): arg is SoccerMatchEvent {
    return arg.__typename === 'SoccerEvent'
  }

  static isConfrontation (arg: AnyEventType): arg is ConfrontationEvent {
    return arg.__typename === 'ConfrontationKind'
  }

  static isMatch (arg: AnyEventType): arg is MatchEvent {
    return arg.__typename === 'MatchKind'
  }

  static isGeneric (arg: AnyEventType): arg is GenericEvent {
    return arg.__typename === 'EventKind'
  }
}

export default OlympicUtils