export interface Transmission {
  url?: any;
  broadcastStatus?: {
    id: string
    label: string
  }
  period?: {
    id: string
    label: string
  }
}

export interface Modality {
  name: string;
  code: string;
}

export interface Category {
  name: string;
  code: string;
}

export interface Sport {
  name: string;
  code: string;
}

export interface GenericEvent extends Event {
  transmission: Transmission;
  summary: string;
  startDate: string;
  startHour: string;
  modality: Modality;
  stage: string;
  venue: string;
  canceled: boolean;
  suspended: boolean;
  decisive: boolean;
  note: string;
}

export type EventTypeName = 'SoccerEvent' | 'EventKind' | 'ConfrontationKind' | 'MatchKind'
export type AnyEventType = SoccerMatchEvent | GenericEvent | ConfrontationEvent | MatchEvent

export interface MatchEvent extends Event {
  transmission: Transmission;
  startDate: string;
  startHour: string;
  modality: Modality;
  category: Category;
  sport: Sport;
  stage: string;
  round: number;
  venue: string;
  canceled: boolean;
  suspended: boolean;
  wo: boolean;
  decisive: boolean;
  note: string;
  winner?: any;
  participants: Participants;
  scoreboard: Scoreboard;
}

export interface Event {
  __typename: EventTypeName
  category: Category;
  sport: Sport;
}

export interface OlympicQueryResponse {
  data: OlympicQueryData
}

export interface OlympicQueryData {
  olympicAgenda: SportOlympicAgenda[]
  brazilOlympicAgenda: BrazilOlympicAgenda
}

export interface BrazilOlympicAgenda {
  now: AnyEventType[]
  past: AnyEventType[]
  future: AnyEventType[]
}

export interface Phase {
  name: string;
  type: string;
}

export interface Team {
  id: number;
  name: string;
  popularName: string;
  badge_svg: string;
  badge_png: string;
}

export interface Scoreboard {
  home?: any;
  away?: any;
  penalty?: Penalty;
}

export interface Match {
  startDate: string;
  startHour: string;
  canceled: boolean;
  suspended: boolean;
  wo: boolean;
  phase: Phase;
  round: number;
  homeTeam: Team;
  awayTeam: Team;
  scoreboard: Scoreboard;
  winner?: any;
  transmission?: any;
}

export interface SoccerMatchEvent extends Event {
  match: Match;
}

export interface Modality {
    name: string;
    code: string;
    __typename: string;
}

export interface Category {
    name: string;
    code: string;
    __typename: string;
}

export interface Sport {
    name: string;
    code: string;
    __typename: string;
}

export interface X1 {
    url: string;
    __typename: string;
}

export interface Png {
    x1: X1;
    __typename: string;
}

export interface Svg {
    url: string;
    __typename: string;
}

export interface Flag {
    png: Png;
    svg: Svg;
}

export interface Represents {
    name: string;
    code: string;
    codePt: string;
    flag: Flag;
}

export interface Winner {
    slug: string;
    name: string;
    popularName: string;
    represents: Represents;
    __typename: string;
}

export interface Participant {
    slug: string;
    name: string;
    popularName: string;
    represents: Represents;
    __typename: string;
}

export interface Participants {
    a: Participant;
    b: Participant;
    __typename: string;
}

export interface Penalty {
    home?: any;
    away?: any;
    __typename: string;
}

export interface ConfrontationEvent extends Event {
  transmission: Transmission;
  startDate: string;
  startHour: string;
  modality: Modality;
  stage: string;
  round: number;
  venue: string;
  canceled: boolean;
  suspended: boolean;
  wo: boolean;
  decisive: boolean;
  note: string;
  winner: Winner;
  participants: Participants;
  scoreboard: Scoreboard;
}

export interface SportOlympicAgenda {
  sport: Sport,
  now: AnyEventType[]
  future: AnyEventType[]
  past: AnyEventType[]
}