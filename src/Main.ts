import { Client } from "discord.js"
import DiscordBot from "./DiscordBot"
import { PrismaClient } from "@prisma/client";

class Main {
  discord
  database: PrismaClient

  constructor () {
    this.discord = new DiscordBot(this)
    this.database = new PrismaClient()
  }

  async init () {
    this.discord.init()
  }
}

export default Main