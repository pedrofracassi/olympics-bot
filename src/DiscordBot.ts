import axios from "axios";
import { ApplicationCommand, Client, Guild, MessageEmbed, Snowflake, TextChannel } from "discord.js";
import Main from "./Main";
import { AnyEventType, OlympicQueryResponse } from "./OlympicQueryData";
import OlympicUtils from "./OlympicUtils";
import getCountryIso2 from "country-iso-3-to-2"

interface UniqueData {
  sport: string
  category: string
  modality: string
  startHour: string
  startDate: string
}

const olympicQueryUrl = process.env.QUERY_URL!

class DiscordBot {
  client
  main

  constructor (main: Main) {
    this.main = main
    this.client = new Client({
      intents: [
        'GUILDS'
      ]
    })
  }

  getMatch (event: AnyEventType): UniqueData {
    if (OlympicUtils.isConfrontation(event)) {
      return {
        category: event.category.name,
        modality: event.modality.name,
        sport: event.sport.name,
        startDate: event.startDate,
        startHour: event.startHour
      }
    }

    if (OlympicUtils.isGeneric(event)) {
      return {
        category: event.category.name,
        modality: event.modality.name,
        sport: event.sport.name,
        startDate: event.startDate,
        startHour: event.startHour
      }
    }

    if (OlympicUtils.isMatch(event)) {
      return {
        category: event.category.name,
        modality: event.modality.name,
        sport: event.sport.name,
        startDate: event.startDate,
        startHour: event.startHour
      }
    }

    return {
      category: event.category.name,
      modality: 'soccer',
      sport: event.sport.name,
      startDate: event.match.startDate,
      startHour: event.match.startHour
    }
  }

  getNotificationEmbed (event: AnyEventType): MessageEmbed {
    let embed = new MessageEmbed()
      .setColor(0x8fc639)
      .setTitle('Evento começando!')

    if (OlympicUtils.isConfrontation(event)) {
      embed
        .setDescription([
          `**${event.sport.name}** - ${event.category.name}${event.modality?.name ? ` - ${event.modality.name}` : ''}`,
          '',
          `:flag_${getCountryIso2(event.participants.a.represents?.code)?.toLowerCase() || 'white'}: ${event.participants.a.popularName || event.participants.a.name}_`,
          `:flag_${getCountryIso2(event.participants.b.represents?.code)?.toLowerCase() || 'white'}: ${event.participants.b.popularName || event.participants.b.name}_`,
          event?.transmission?.url ? `\n__**[Assistir](${event?.transmission?.url})**__` : '',
        ].join('\n'))
    }

    if (OlympicUtils.isGeneric(event)) {
      embed
      .setDescription([
        `**${event.sport.name}** - ${event.category.name}${event.modality?.name ? ` - ${event.modality.name}` : ''}`,
        event?.transmission?.url ? `\n__**[Assistir](${event?.transmission?.url})**__` : '',
      ].join('\n'))
    }

    if (OlympicUtils.isMatch(event)) {
      embed
      .setDescription([
        `**${event.sport.name}** - ${event.category.name}${event.modality?.name ? ` - ${event.modality?.name}` : ''}`,
        '',
        `:flag_${getCountryIso2(event.participants.a.represents?.code)?.toLowerCase() || 'white'}: ${event.participants.a.popularName || event.participants.a.name}_`,
        `:flag_${getCountryIso2(event.participants.b.represents?.code)?.toLowerCase() || 'white'}: ${event.participants.b.popularName || event.participants.b.name}_`,
        event?.transmission?.url ? `\n__**[Assistir](${event?.transmission?.url})**__` : '',
      ].join('\n'))
    }

    if (OlympicUtils.isSoccerEvent(event)) {
      embed
      .setDescription([
        `**${event.sport.name}** - ${event.category.name}`,
        '',
        `${event.match.homeTeam.popularName || event.match.homeTeam.name}_`,
        `${event.match.awayTeam.popularName || event.match.awayTeam.name}_`,
        event.match.transmission?.url ? `\n__**[Assistir](${event.match.transmission?.url})**__` : '',
      ].join('\n'))
    }

    return embed
  }

  async notifyTick () {
    const cal = await axios.get<OlympicQueryResponse>(olympicQueryUrl).then(res => res.data.data)

    for (const event of cal.brazilOlympicAgenda.now) {
      const entry = await this.main.database.live_notification.findUnique({
        where: {
          sport_category_modality_startHour_startDate: this.getMatch(event)
        }
      }).catch(e => {})

      if (!entry) {
        await this.main.database.live_notification.create({
          data: this.getMatch(event)
        })

        const channels = await this.main.database.subscription.findMany({})

        for (const channelInfo of channels) {
          const guild = await this.client.guilds.fetch(channelInfo.guild_id as Snowflake).catch(e => {})
          if (!guild) continue
          const channel = await guild.channels.fetch(channelInfo.channel_id as Snowflake).catch(e => {}) as TextChannel
          if (!channel) continue

          console.log(`Sending event to #${channel.name}`)
          channel.send({
            embeds: [
              this.getNotificationEmbed(event)
            ]
          })
        }
      }
    }
  }

  async init () {
    console.log('Initializing Discord bot')

    this.client.on('debug', console.debug)

    this.client.on('ready', async () => {
      console.log('Ready!')

      setInterval(() => {
        this.notifyTick()
      }, 1 * 60 * 1000)

      if (process.env.SETUP) {
        await this.client.application?.commands.create({
          name: 'notificar',
          description: 'Habilita as notificações dos eventos'
        }).then(() => console.log('created'))
    
        await this.client.application?.commands.create({
          name: 'calendario',
          description: 'Mostra o calendário do Brasil nas olimpíadas hoje'
        }).then(() => console.log('created'))
      }
    })

    this.client.on('interaction', async interaction => {
      if (interaction.isCommand()) {
        switch (interaction.commandName) {
          case 'notificar': {
            const entry = await this.main.database.subscription.findUnique({
              where: {
                channel_id: interaction.channel!.id
              }
            })
            if (entry) {
              await this.main.database.subscription.delete({
                where: {
                  channel_id: interaction.channel!.id
                }
              })
              interaction.reply('❌ **Não enviaremos mais notificações nesse canal.**')
            } else {
              await this.main.database.subscription.create({
                data: {
                  channel_id: interaction.channel!.id,
                  guild_id: interaction.guild!.id
                }
              })
              interaction.reply('✅ **Enviaremos notificações nesse canal quando os eventos estiverem prestes a começar.**')
            }
            break
          }
          case 'calendario': {
            const cal = await axios.get<OlympicQueryResponse>(olympicQueryUrl).then(res => res.data.data)
            
            const everything = [
              ...cal.brazilOlympicAgenda.now,
              ...cal.brazilOlympicAgenda.future
            ]

            interaction.reply({
              embeds: [
                new MessageEmbed()
                .setColor(0x8fc639)
                .setTitle('Próximos eventos do Brasil')
                .setDescription(everything.map(e => {
                  if (OlympicUtils.isGeneric(e)) {
                    return [
                      `\`${e.startHour.slice(0, 5)}\` - **${e.sport.name}**${e.transmission?.url ? ` - **[Assistir](${e.transmission?.url})**` : ''}`,
                      `${e.category.name} - ${e.modality.name}`
                    ].join('\n')
                  } 
                  
                  if (OlympicUtils.isMatch(e)) {
                    return [
                      `\`${e.startHour.slice(0, 5)}\` - **${e.sport.name}**${e.transmission?.url ? ` - **[Assistir](${e.transmission?.url})**` : ''}`,
                      `${e.category.name} - ${e.modality.name}`,
                      [
                        `:flag_${getCountryIso2(e.participants.a.represents?.code)?.toLowerCase() || 'white'}: _${e.participants.a.name}_`,
                        `_${e.participants.b.name}_ :flag_${getCountryIso2(e.participants.b.represents?.code)?.toLowerCase() || 'white'}:`
                      ].join(' vs ')
                    ].join('\n')
                  }
                  
                  if (OlympicUtils.isConfrontation(e)) {
                    return [
                      `\`${e.startHour.slice(0, 5)}\` - **${e.sport.name}**${e.transmission?.url ? ` - **[Assistir](${e.transmission?.url})**` : ''}`,
                      `${e.category.name} - ${e.modality.name}`,
                      [
                        `:flag_${getCountryIso2(e.participants.a.represents.code)?.toLowerCase() || 'white'}: _${e.participants.a.name}_`,
                        `_${e.participants.b.name}_ :flag_${getCountryIso2(e.participants.b.represents.code)?.toLowerCase() || 'white'}:`
                      ].join(' vs ')
                    ].join('\n')
                  }
                  
                  if (OlympicUtils.isSoccerEvent(e)) {
                    return [
                      `\`${e.match.startHour.slice(0, 5)}\` - **${e.sport.name}**${e.match.transmission?.url ? ` - **[Assistir](${e.match.transmission?.url})**` : ''}`,
                      `${e.category.name} - ${e.match.homeTeam.name} vs ${e.match.awayTeam.name}`
                    ].join('\n')
                  } 
                }).concat('_[Desenvolvido por Pedro Fracassi](https://fracassi.tech/)_').join('\n\n'))
              ]
            })

            break
          }
        }
      }
    })

    this.client.login(process.env.DISCORD_TOKEN)
  }
}

export default DiscordBot